import {Routes} from '@angular/router';

import {FormComponent} from './pages/form/form.component';
import {ClienteComponent} from './pages/cliente/cliente.component';

export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'form', pathMatch: 'full'},
  {path: 'form', component: FormComponent},
  {path: 'cliente', component: ClienteComponent}
];
